swal (
    title:'Successfull!'
    text:'User data has been updated from the system.', 
    type:'success',
    preConfirm: ->
        $("#view_user_info").show()
        $("#eidt_user_info").hide()
        $("#edit-btn").show()
        $("#view_user_info_name").html('<%=@user.full_name%>')
        $("#view_user_info_left_card_name").html('<%=@user.full_name%>')
        $("#view_user_info_left_card_position").html('<%=@user.position%>')
        $("#view_user_info_birth_date").html('<%=@user.date_of_birth.to_s.to_date%>')
        $("#view_user_info_phone").html('<%=@user.phone.to_s%>')
        $("#view_user_info_left_card_phone").html('<%=@user.position.to_s%>')
        $("#view_user_info_location").html('<%=@user.address.to_s%>')
        $("#view_user_info_left_card_address").html('<%=@user.address.to_s%>')
        $("#view_user_info_left_card_desc").html('<%=@user.description.to_s%>')
        $("#view_user_info_province").html('<%=@province[0].province_name.to_s%>')
        $("#view_user_info_country").html('<%=@country[0].country_name.to_s%>')
        $("#view_user_info_gender_tr").empty()
        $('<th scope="row">Gender</th><td><%=@gender.to_s%></td>').appendTo("#view_user_info_gender_tr")
    )