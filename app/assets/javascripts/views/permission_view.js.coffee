window.Views.Permission ||={}
class Views.Permission extends Views.ApplicationView
  constructor: () ->
    _initialize()

  @deletePermission = (id) ->
    $this_row = this
    swal(
      title: 'Bạn chắc chắn xoá?'
      text: 'Dữ liệu sau khi xoá sẽ không thể phục hồi!'
      type: 'warning'
      showCancelButton: true
      confirmButtonText: 'Đồng ý xoá'
      cancelButtonText: 'Huỷ xoá'
      confirmButtonClass: 'btn btn-success'
      cancelButtonClass: 'btn btn-danger mergin-left-btn-swal'
      showLoaderOnConfirm: true,
      preConfirm: ->
        promise = new Promise((resolve) ->
          request = Daos.Permission.deletePermission(id)
          request.done (data) ->
            console.log data
            $('#permission_id_'+id).remove()
            swal 'Xoá thành công!', 'Dữ liệu Permission đã được xoá khỏi hệ thống.', 'success'
          request.fail (data) ->
            gift_string = ""
            gift_string = data.responseText.replace(/"|\[|\]/g,'').replace(/,/g, ", ")
            swal 'Xoá thất bại!', 'Xoá Permission không thành công!<br> Các quà thuộc loại quà này: error'
        )
        promise
      buttonsStyling: true).then (result) ->
        unless result.value
          swal 'Huỷ xoá!', 'Dữ liệu của bạn đã an toàn', 'error'
        return
    
  @permissionInfo = (id) ->
    request = Daos.Permission.permissionInfo(id)
    request.  done (data) ->
      console.log data.permissions
      $("#edit_permission_id").val(data.permissions.id)
      $("#edit_permission_name").val(data.permissions.permission_name)
      $("#edit_permission_code").val(data.permissions.permission_code)
      $("#edit_permission_desc").val(data.permissions.description)
      $('#view_permission_groups').empty()
      console.log data.groups
      for group in data.groups
        res ='<li id="groups_id_group_id" data-toggle="modal"><i class="fa fa-group"></i><h6>group_name</h6><h6>description</h6></li>'
        res = res.replace(/group_id/g, group.id)
        res = res.replace(/group_name/g, group.group_name)
        res = res.replace(/description/g, group.description)
        $(res).appendTo '#view_permission_groups'
     

 

  