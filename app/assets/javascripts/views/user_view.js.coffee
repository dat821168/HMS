window.Views.User ||={}
class Views.User extends Views.ApplicationView
  constructor: () ->
    _initialize()
  
  @lockScreen = () ->
    $("#screen").hide()
    $("#clock_screen").css('display','flex')

  @unclockscreen = () ->
    $("#unclock_screen_button").html("UNLOCK...")
    pass = $("#unclock_screen_password").val()
    request = Daos.User.unclockscreen(pass)
    request.done (data) -> 
      $("#unclock_screen_button").html("UNLOCK")
      console.log data
      if data == true
        $("#unclock_screen_password").val("")
        $("#error").html("")
        $("#screen").show()
        $("#clock_screen").css('display','none')
      else
        $("#unclock_screen_password").val("")
        $("#error").html("Password is incorrect")

  @getProvince = (id) ->
    request = Daos.User.getProvinces(id)
    request.done (data) -> 
      console.log data
      $("select#user_province_id").empty()
      for province in data
        row = "<option value=\"" + province.id + "\">" + province.province_name + "</option>"
        $(row).appendTo("select#user_province_id")

  @updateUserInfo = (user_request) ->
    if user_request == 1
      $("#view_user_info").hide()
      $("#eidt_user_info").show()
      $("#edit-btn").hide()
      
    else
      $("#view_user_info").show()
      $("#eidt_user_info").hide()
      $("#edit-btn").show()