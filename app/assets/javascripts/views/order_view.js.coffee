window.Views.Order ||={}
class Views.Order extends Views.ApplicationView
    constructor: () ->
        _initialize()

    _initialize = ->

    @bookingRoom    = (id) ->
        checkIn = $('#checkin').val()
        checkOut = $('#checkout').val()
        $('#room_id').val(id)
        $('#start_date').val(checkIn)
        $('#end_date').val(checkOut)
        $('#order-code').val("OD"+ Date.now())
        $('#booking_room').modal({show:true})

    @selectRoom     = (id) ->
        request = Daos.Order.selectRoom(id)
        request.done (data) ->
            $("#select-room").empty()
            if data.length == 0
                $this_row = this
                swal(
                        title: 'Status'
                        text: 'Hết phòng'
                        type: 'warning'
                    )
                return
            
            for item in data
                row = '<div class="col-xl-2" style="margin-top: 20px;">
                         <a>
                            <div class="card" style="background-color: room_color"  onclick="window.Views.Order.bookingRoom(room_id)">
                                <div class="card-block">
                                    <div class="side-box " style="background-color: white">
                                        room_icon
                                    </div>
                                    <div class="text-center">
                                        <i class="icofont icofont-home" style="color: white"></i>
                                        <h2 class="counter" style="color: white">room_code</h2>
                                        <span style="color: white">room_status</span>
                                    </div><!-- end of text-center -->    
                                </div><!-- end of card-block -->    
                            </div><!-- end of card -->    
                        </a>
                    </div>'
                if item.room_type_id == 1
                    row = row.replace(/room_icon/g,'<i class="icofont icofont-bed" style="color: #808080"></i>')
                    row = row.replace(/room_color/g,"#808080")
                else
                    row = row.replace(/room_icon/g,'<i class="icofont icofont-royal" style="color: #FFDF00"></i>')
                    row = row.replace(/room_color/g,"#DAA520")
                row = row.replace(/room_code/g,item.code)
                row = row.replace(/room_status/g,item.status)
                row = row.replace(/room_id/g,item.id)
                $(row).appendTo("#select-room")
                $("#customer_name").val("")
                $("#customer_ID").val("")
                $("#customer_address").val("")
                $("#customer_phone").val("")
        return
    
    @viewInfoOrder  = (id) ->
        request = Daos.Order.viewInfoOrder(id)
        request.done (data) ->
            $('#order_id').val(data.odDetailInfo.id)
            $('#customer_name').val(data.odDetailInfo.customer_name)
            $('#check_in').val(data.odDetailInfo.start_date.substr(0,16).replace /T/, " ")
            $('#customer_ID').val(data.odDetailInfo.customer_ID)
            if data.odDetailInfo.end_date == null
                $('#check_out').val("")
            else
                $('#check_out').val(data.odDetailInfo.end_date.substr(0,16).replace /T/, " ")
            $('#customer_address').val(data.odDetailInfo.customer_address)
            $('#payment').val("Chưa thanh toán")
            $('#customer_phone').val(data.odDetailInfo.customer_phone)
            $('#total').val(data.odDetailInfo.total)
            $('#viewInfoOrder').modal({show:true})
        return

    # Sort OrderCode
    @sortOrderCode  = () ->
        valueODCode = $('#orderCode').val()
        if valueODCode == "1"
            $('#orderCode').val("0")
        else
            $('#orderCode').val("1")
        $('#loading').css("display","block")
        $('#roomType').val("0")
        $('#roomCode').val("0")
        $('#userId').val("0")
        $('#created').val("0")
        $('#updated').val("0")
        request = Daos.Order.sortOrderCode(valueODCode)
    #Sort Room Code
    @sortRoomCode   = () ->
        valueODCode = $('#roomCode').val()
        if valueODCode == "1"
            $('#roomCode').val("0")
        else
            $('#roomCode').val("1")
        $('#loading').css("display","block")
        $('#roomType').val("0")
        $('#orderCode').val("0")
        $('#userId').val("0")
        $('#created').val("0")
        $('#updated').val("0")
        request = Daos.Order.sortRoomCode(valueODCode)
    #Sort Creator
    @sortUserId     = () ->
        valueODCode = $('#userId').val()
        if valueODCode == "1"
            $('#userId').val("0")
        else
            $('#userId').val("1")
        $('#loading').css("display","block")
        $('#roomType').val("0")
        $('#roomCode').val("0")
        $('#orderCode').val("0")
        $('#created').val("0")
        $('#updated').val("0")
        request = Daos.Order.sortUserId(valueODCode)
    #Sort Created date
    @sortCreated    = () ->
        valueODCode = $('#created').val()
        if valueODCode == "1"
            $('#created').val("0")
        else
            $('#created').val("1")
        $('#loading').css("display","block")
        $('#roomType').val("0")
        $('#roomCode').val("0")
        $('#userId').val("0")
        $('#orderCode').val("0")
        $('#updated').val("0")
        request = Daos.Order.sortCreated(valueODCode) 
    #Sort Update date
    @sortUpdated    = () ->
        valueODCode = $('#updated').val()
        if valueODCode == "1"
            $('#updated').val("0")
        else
            $('#updated').val("1")
        $('#loading').css("display","block")
        $('#roomType').val("0")
        $('#roomCode').val("0")
        $('#userId').val("0")
        $('#created').val("0")
        $('#orderCode').val("0")
        request = Daos.Order.sortUpdated(valueODCode)   

            


    

    
                

        
