window.Views.Room ||={}
class Views.Room extends Views.ApplicationView
    constructor: () ->
        _initialize()

    _initialize = ->
    
    @bookingRoom = () ->
        room_id = $("#room_info_id").val()
        room_code = $("#room_info_code").val()
        room_type = $("#room_info_type").val()
        newWindow = window.open("/order_room/new",'_self')
        window.Views.Order.bookingRoom(room_id,room_code,room_type)

   
    @updateRoomStatus = (status) ->
        room_id = $("#room_info_id").val()
        request = Daos.Room.updateRoomStatus(room_id,status)
        request.done ->
        Views.Room.updateStatusButton(status)
        Views.Room.updateRoomCard(room_id,status)
   
    @showroominfo = (id) ->
        $("#view_room_booking_button").hide()
        $("#view_room_checkout_button").hide()
        $("#room_info_table").hide()
        $('#room_Modal').modal({show:true})
        $('#loader_show_room').show()
        request = Daos.Room.showroominfo(id)
        request.done (data) -> 
            $('#loader_show_room').hide()
            $("#room_info_table").show()
            $("#room_info_code").html(data.code)
            $("#room_info_desc").html(data.description)
            $("#room_info_id").val(data.id)
            Views.Room.updateStatusButton(data.status)
            if data.room_type_id == 1
                $("#room_info_type").html("Normal")
                $("#room_info_type").css('color','#808080')
            else if data.room_type_id == 2
                $("#room_info_type").html("VIP")
                $("#room_info_type").css('color','#FFDF00')

    @updateRoomCard = (id,status) ->
        if status == "available"
            $('#room_'+id).css('background-color','#228B22')
            $('#room_status_'+id).html('available')
        else if status == "occupied"
            $('#room_'+id).css('background-color','#FF8C00')
            $('#room_status_'+id).html('occupied')
        else if status == "problem"
            $('#room_'+id).css('background-color','#F90F0F')
            $('#room_status_'+id).html('problem')
        else if status == "clean" 
            $('#room_'+id).css('background-color','#3A6EFF')
            $('#room_status_'+id).html('clean')


    @updateStatusButton = (status) ->
        if status == "available"
            $("#button_show_room_available").prop('disabled',true)
            $("#button_show_room_notavailable").prop('disabled',false)
            $("#button_show_room_problem").prop('disabled',false)
            $("#button_show_room_clean").prop('dicysabled',false)
            $("#button_show_room_available").css('background-color','#808080')
            $("#button_show_room_notavailable").css('background-color','#FF8C00')
            $("#button_show_room_problem").css('background-color','#F90F0F')
            $("#button_show_room_clean").css('background-color','#3A6EFF')
            $("#view_room_booking_button").show()
            $("#view_room_checkout_button").hide()
        else if status == "occupied"
            $("#button_show_room_notavailable").prop('disabled',true)
            $("#button_show_room_problem").prop('disabled',false)
            $("#button_show_room_clean").prop('dicysabled',false)
            $("#button_show_room_available").prop('disabled',false)
            $("#button_show_room_notavailable").css('background-color','#808080')
            $("#button_show_room_problem").css('background-color','#F90F0F')
            $("#button_show_room_clean").css('background-color','#3A6EFF')
            $("#button_show_room_available").css('background-color','#228B22')
            $("#view_room_booking_button").hide()
            $("#view_room_checkout_button").show()
        else if status == "problem"
            $("#button_show_room_problem").prop('disabled',true)
            $("#button_show_room_available").prop('disabled',false)
            $("#button_show_room_notavailable").prop('disabled',false)
            $("#button_show_room_clean").prop('dicysabled',false)
            $("#button_show_room_available").css('background-color','#228B22')
            $("#button_show_room_notavailable").css('background-color','#FF8C00')
            $("#button_show_room_problem").css('background-color','#808080')
            $("#button_show_room_clean").css('background-color','#3A6EFF')
            $("#view_room_booking_button").hide()
            $("#view_room_checkout_button").hide()
        else if status == "clean" 
            $("#button_show_room_clean").prop('disabled',true)
            $("#button_show_room_available").prop('disabled',false)
            $("#button_show_room_notavailable").prop('disabled',false)
            $("#button_show_room_problem").prop('disabled',false)
            $("#button_show_room_available").css('background-color','#228B22')
            $("#button_show_room_notavailable").css('background-color','#FF8C00')
            $("#button_show_room_problem").css('background-color','#F90F0F')
            $("#button_show_room_clean").css('background-color','#808080')
            $("#view_room_booking_button").hide()
            $("#view_room_checkout_button").hide()
