﻿window.Views.UserGroup ||={}
class Views.UserGroup extends Views.ApplicationView
  constructor: () ->
    _initialize()

  _initialize = ->

  @deleteUserGroup = (id) ->
    $this_row = this
    swal(
      title: 'Bạn chắc chắn xoá?'
      text: 'Dữ liệu sau khi xoá sẽ không thể phục hồi!'
      type: 'warning'
      showCancelButton: true
      confirmButtonText: 'Đồng ý xoá'
      cancelButtonText: 'Huỷ xoá'
      confirmButtonClass: 'btn btn-success'
      cancelButtonClass: 'btn btn-danger mergin-left-btn-swal'
      showLoaderOnConfirm: true,
      preConfirm: ->
        promise = new Promise((resolve) ->
          request = Daos.UserGroup.deleteUserGroup(id)
          request.done (data) ->
            console.log data
            $('#groups_id_'+id).remove()
            swal 'Xoá thành công!', 'Dữ liệu group đã được xoá khỏi hệ thống.', 'success'
          request.fail (data) ->
            gift_string = ""
            gift_string = data.responseText.replace(/"|\[|\]/g,'').replace(/,/g, ", ")
            swal 'Xoá thất bại!', 'Xoá Group không thành công!<br> Các quà thuộc loại quà này: error'
        )
        promise
      buttonsStyling: true).then (result) ->
        unless result.value
          swal 'Huỷ xoá!', 'Dữ liệu của bạn đã an toàn', 'error'
        return
  @deleteUserGroupPermission = (id) ->
    $this_row = this
    swal(
      title: 'Bạn chắc muốn hủy permission này?'
      text: 'Dữ liệu sau khi hủy sẽ không thể phục hồi!'
      type: 'warning'
      showCancelButton: true
      confirmButtonText: 'Đồng ý xoá'
      cancelButtonText: 'Huỷ xoá'
      confirmButtonClass: 'btn btn-success'
      cancelButtonClass: 'btn btn-danger mergin-left-btn-swal'
      showLoaderOnConfirm: true,
      preConfirm: ->
        promise = new Promise((resolve) ->
          request = Daos.UserGroup.deleteUserGroupPermission(id)
          request.done (data) ->
            console.log data
            $('#group_permissions_id_'+id).remove()
            swal 'Hủy thành công!', 'Group đã được hủy permission.', 'success'
          request.fail (data) ->
            gift_string = ""
            gift_string = data.responseText.replace(/"|\[|\]/g,'').replace(/,/g, ", ")
            swal 'Xoá thất bại!', 'Hủy Permission không thành công!<br> Các quà thuộc loại quà này: error'
        )
        promise
      buttonsStyling: true).then (result) ->
        unless result.value
          swal 'Giử Permission!', 'Dữ liệu của bạn đã an toàn', 'error'
        return
 
  @userGroupInfo = (id) ->
    request = Daos.UserGroup.userGroupInfo(id)
    request.done (data) ->
      $("#edit_group_id").val(data.id)
      $("#edit_group_name").val(data.group_name)
      $("#edit_group_desc").val(data.description)
  
  @showPermission = (id) ->
    request = Daos.UserGroup.showPermission(id)
    request.done (data) -> 
      $("#add_group_permission_name").val(data.group.group_name)
      $("#add_group_permission_id").val(data.group.id)
      $('#group_permissions').empty()
      $('#add_permissions').empty()
      for permission in data.list_permissions
        res= '<li id="group_permissions_id_permission.id">'+
                '<div class="m-t-15" style="float: right;">'+
                  '<a class="tabledit-delete-button btn btn-danger waves-effect waves-light" data-toggle="tooltip" onclick="window.Views.UserGroup.deleteUserGroupPermission(permission.id)" title="Delete Permission"><span class="icofont icofont-ui-delete" style="color: white"></span></a>'+
                '</div>'+
                '<i class="fa fa-angle-double-right"></i>'+
                '<h6> permission_name &nbsp;- &nbsp; permission_code </h6>'+
                '<h6> permission.description </h6>'+
             '</li>'
        res = res.replace(/permission.id/g, permission.id)
        res = res.replace(/permission_name/g, permission.permission_name)
        res = res.replace(/permission_code/g, permission.permission_code)
        res = res.replace(/permission.description/g, permission.description)
        $(res).appendTo '#group_permissions'
      for permission in data.permissions
        res= '<li id="group_permissions_id_permission.id">'+
                '<div class="m-t-15" style="float: right;">'+
                  '<a class="tabledit-delete-button btn btn-danger waves-effect waves-light" data-toggle="tooltip" onclick="window.Views.UserGroup.deleteUserGroupPermission(permission.id)" title="Delete Permission"><span class="icofont icofont-ui-delete" style="color: white"></span></a>'+
                '</div>'+
                '<i class="fa fa-angle-double-right"></i>'+
                '<h6> permission_name &nbsp;- &nbsp; permission_code </h6>'+
                '<h6> permission.description </h6>'+
             '</li>'
        res = res.replace(/permission.id/g, permission.id)
        res = res.replace(/permission_name/g, permission.permission_name)
        res = res.replace(/permission_code/g, permission.permission_code)
        res = res.replace(/permission.description/g, permission.description)
        $(res).appendTo '#add_permissions'
        
     


  