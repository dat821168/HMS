window.Controllers ||= {}
class Controllers.ApplicationController
  constructor: () ->
    _initialize()

  _initialize = ->
    application_view = new Views.ApplicationView()
