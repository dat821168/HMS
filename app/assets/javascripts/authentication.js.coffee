# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.coffee.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require jquery
#= require jquery_ujs
#= require jquery-ui/js/jquery-ui.min
#= require popper.js/js/popper.min
#= require waves/js/waves.min
#= require bootstrap.min
#= require elements
#= require main
