window.Daos.Order ||={}
class Daos.Order extends Daos.ApplicationDao
  constructor: () ->

  @selectRoom:(id)      ->
    request = $.get('/order_room/'+id+'/selectRoom')
    request
  
  @viewInfoOrder:(id)   ->
    request = $.get('/order_room/'+id+'/viewInfoOrder')
    request
  
  #Sort 
  @sortOrderCode:(id)   ->
    request = $.get('/order_room/'+id+'/sortOrderCode')
  
  @sortRoomCode:(id)    ->
    request = $.get('/order_room/'+id+'/sortRoomCode')
  
  @sortUserId:(id)      ->
    request = $.get('/order_room/'+id+'/sortUserId')
  
  @sortCreated:(id)     ->
    request = $.get('/order_room/'+id+'/sortCreated')
  
  @sortUpdated:(id)     ->
    request = $.get('/order_room/'+id+'/sortUpdated')
  