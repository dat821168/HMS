window.Daos.Permission ||={}
class Daos.Permission extends Daos.ApplicationDao
    constructor: () ->

    @deletePermission:(id) ->
        request = $.get('/admin/'+id+'/destroy_permission')
        request
     
    @permissionInfo:(id) ->
        request = $.get('/admin/'+id+'/permission_info')
        request