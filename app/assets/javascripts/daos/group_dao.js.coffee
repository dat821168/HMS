﻿window.Daos.UserGroup ||={}
class Daos.UserGroup extends Daos.ApplicationDao
  constructor: () ->

  @deleteUserGroup:(id) ->
    request = $.get('/admin/'+id+'/destroygroup')
    request
  
  @deleteUserGroupPermission:(id) ->
    request = $.get('/admin/'+id+'/destroy_group_permission')
    request

  @userGroupInfo:(id) ->
    request = $.get('/admin/'+id+'/user_group_info')
    request
    
  @showPermission:(id) ->
    request = $.get('/admin/'+id+'/show_group_permission')
    request