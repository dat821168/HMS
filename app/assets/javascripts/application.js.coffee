# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
# vendor/assets/javascripts directory can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file. JavaScript code in this file should be added after the last require_* statement.
#
# Read Sprockets README (https:#github.com/rails/sprockets#sprockets-directives) for details
# about supported directives.
#
#= require activestorage
#= require cable
#= require jquery
#= require jquery_ujs
#= require jquery-ui/js/jquery-ui.min
#= require tether/js/tether.min
#= require popper.js/js/popper.min
#= require waves/js/waves.min
#= require bootstrap.min
#= require jquery-slimscroll/js/jquery.slimscroll
#= require jquery.nicescroll/js/jquery.nicescroll.min
#= require classie/js/classie
#= require notification/js/bootstrap-growl.min
#= require main
#= require elements
#= require menu.min
#= require sweetalert2.all.min
#= require_tree .

$ ->
    setTimeout ( ->
        d = new Date()
        n = d.toJSON().replace /Z/, "-07:00"
        date = new Date(n)
        v = date.toJSON().substr(0,16)
        nn = d.toJSON().replace /Z/, "-12:00"
        date2 = new Date(nn)
        vv = date2.toJSON().substr(0,16)
        $('#checkin').val v
        $('#checkout').val vv
    ),1

    setInterval (->
        d = new Date()
        n = d.toJSON().replace /Z/, "-07:00"
        date = new Date(n)
        v = date.toJSON().substr(0,16)
        nn = d.toJSON().replace /Z/, "-12:00"
        date2 = new Date(nn)
        vv = date2.toJSON().substr(0,16)
        $('#checkin').val v
        $('#checkout').val vv
        return
    ), 30000
    return

    


