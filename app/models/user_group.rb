class UserGroup < ApplicationRecord
    has_many :permission_per_groups
    has_many :user_per_groups
end
