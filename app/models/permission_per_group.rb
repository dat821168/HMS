class PermissionPerGroup < ApplicationRecord
    belongs_to :permission
    belongs_to :user_group
end
