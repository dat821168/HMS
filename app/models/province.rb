class Province < ApplicationRecord
    belongs_to :country
    has_many :cv_user_infos
end
