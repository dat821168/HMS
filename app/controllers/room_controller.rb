class RoomController < ApplicationController
    before_action :authenticate_user!
    def index
        @rooms = Room.all.order(:id).page params[:page]
    end
    def show
        sleep(1)
        unless params[:id]
            render json: {response: 'error'}, status: 400
            return
        end
        begin
            @room = Room.find_by_id(params[:id])
            render json: @room.to_json, status: 200
        rescue => exception
            render json: {response: 'error'}, status: 500
        end
    end

    def update_room_status
        unless params[:id]
            render json: {response: 'error'}, status: 400
            return
        end
        begin
            p @room = Room.find_by_id(params[:id])
            p @room.status = params[:status]
            if @room.save
                render json: {response: 'Successful'}, status: 200
            end
        rescue => exception
            p @room
            render json: {response: 'error'}, status: 500
        end
    end

    # *******************************************************************************************
    # *                                                                                         *
    # *                         Get params information controller                               *
    # *                                                                                         *
    # *******************************************************************************************

    private
    def room_params
        params.require(:room).permit(:id, :code, :status, :room_type_id, :description)
    end
    
end
