class UserController < ApplicationController
    before_action :authenticate_user!
    def index
        @country = Country.where(id: current_user.country_id)
        @province = Province.where(id: current_user.province_id)
        @countries = Country.all
        @provinces = Province.all
    end
    def update
        p @user = User.find(user_params[:id]) 
        respond_to do |format|
            @user.attributes = user_params
            @country = Country.where(id:  @user.country_id)
            @province = Province.where(id:  @user.province_id)
            if(@user.gender == 1)
                @gender ="Male"
            else
                @gender = "Female"
            end
            if @user.save
                format.js
            else
            end
        end
    end

    def get_provinces
        unless params[:id]
            render json: {response: 'error'}, status: 400
            return
        end
        begin
            @provices = Province.where(country_id: params[:id])
            render json: @provices.to_json, status: 200
        rescue => exception
            render json: {response: 'error'}, status: 500
        end
    end

    def verify_password
        unless params[:pass]
            render json: {response: 'error'}, status: 400
            return
        end
        begin
            @result = current_user.valid_password?(params[:pass])
            render  json: @result.to_json,  status: 200
        rescue => exception
            render json: {response: 'error'}, status: 500
        end
    end

    private
    def user_params
        params.require(:user).permit(:id, :full_name, :position, :address, :phone, :gender, :date_of_birth,:province_id,:country_id, :description)
    end
end
