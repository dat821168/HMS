class OrderRoomController < ApplicationController
    def index
        @lOrder = Order.order("order_code DESC")
        @lUser = User.all
        @lRoom = Room.all
        @lRoomType = RoomType.all
        @lOrderDetail = OrderDetail.all
    end
    def new
        @rEmpty = Room.where(:room_type_id => 1, :status => 'available')
        @rType = RoomType.all
    end

    def selectRoom
        unless params[:id]
            render json: {response: 'error'}, status: 400
            return
        end
        begin
            @rEmpty = Room.where(:room_type_id => params[:id], :status => 'available')
            render json: @rEmpty.to_json, status: 200
        rescue => exception
            render json: {response: 'error'}, status: 500
        end
    end

    def createOrder
        @crOrder = Order.new(order_params)
        respond_to do |format|    
            if @crOrder.save 
                @crOrderDetails = OrderDetail.new(order_detail_params)
                @crOrderDetails.order_id = @crOrder.id
                if @crOrderDetails.save
                    p @rType_Select = RoomType.find(Room.find(@crOrder.room_id).room_type_id).id
                    @rType = RoomType.all
                    @rUpdate = Room.find_by_id(@crOrder.room_id)
                    @rUpdate.status = "notavailable"
                    @rUpdate.save
                    format.js {}
                else
                    p @crOrderDetails.errors.full_messages
                    error.js {}
                end
            else
                p @crOrder.errors.full_messages
                error.js {}
            end
        end   
    end

    def viewInfoOrder
        unless params[:id]
            render json: {response: 'error'}, status: 400
            return
        end
        begin
            @odInfo = Order.find_by_id(params[:id])
            @odDetailInfo = OrderDetail.find_by_id(@odInfo.id)
            @rInfo = Room.find_by_id(@odInfo.room_id)
            @rTypeInfo = RoomType.find_by_id(@rInfo.room_type_id)
            @uInfo = User.find_by_id(@odInfo.user_id)
            render json: {odInfo: @odInfo, odDetailInfo: @odDetailInfo, rInfo: @rInfo, rTypeInfo: @rTypeInfo, uInfo: @uInfo}.to_json, status: 200
        rescue => exception
            render json: {response: 'error'}, status: 500
        end
    end
    
    def editInfoOrder
        sleep(3)
        @edInfoOrder = OrderDetail.find_by_id(edInfoOrder_params[:id])
        respond_to do |format|
            @edInfoOrder.attributes = edInfoOrder_params
            if @edInfoOrder.save
                format.js {}
            else
                p @edInfoOrder.errors.full_messages
                format.js {}
            end
        end
    end

    # Sort
    def sortOrderCode
        begin
            respond_to do |format|
                if params[:id] == "0"
                    @lOrder = Order.order("order_code ASC")
                end
                if params[:id] == "1"                   
                    @lOrder = Order.order("order_code DESC")
                end
                @lUser = User.all
                @lRoom = Room.all
                @lRoomType = RoomType.all
                @lOrderDetail = OrderDetail.all
                format.js {}
            end
        end
    end

    def sortRoomCode
        begin
            respond_to do |format|
                if params[:id] == "0"
                    @lOrder = Order.order("room_id ASC")
                end
                if params[:id] == "1"                   
                    @lOrder = Order.order("room_id DESC")
                end
                @lUser = User.all
                @lRoom = Room.all
                @lRoomType = RoomType.all
                @lOrderDetail = OrderDetail.all
                format.js {}
            end
        end
    end

    def sortUserId
        begin
            respond_to do |format|
                if params[:id] == "0"
                    @lOrder = Order.order("user_id ASC")
                end
                if params[:id] == "1"                   
                    @lOrder = Order.order("user_id DESC")
                end
                @lUser = User.all
                @lRoom = Room.all
                @lRoomType = RoomType.all
                @lOrderDetail = OrderDetail.all
                format.js {}
            end
        end
    end

    def sortCreated
        begin
            respond_to do |format|
                if params[:id] == "0"
                    @lOrder = Order.order("created_at ASC")
                end
                if params[:id] == "1"                   
                    @lOrder = Order.order("created_at DESC")
                end
                @lUser = User.all
                @lRoom = Room.all
                @lRoomType = RoomType.all
                @lOrderDetail = OrderDetail.all
                format.js {}
            end
        end
    end

    def sortUpdated
        begin
            respond_to do |format|
                if params[:id] == "0"
                    @lOrder = Order.order("updated_at ASC")
                end
                if params[:id] == "1"                   
                    @lOrder = Order.order("updated_at DESC")
                end
                @lUser = User.all
                @lRoom = Room.all
                @lRoomType = RoomType.all
                @lOrderDetail = OrderDetail.all
                format.js {}
            end
        end
    end

    private

    def order_params
        p params.require(:createOrder).permit(:id, :order_code, :room_id, :user_id)
    end

    def order_detail_params
        p params.require(:createOrder).permit(:id, :order_id ,:order_code, :start_date, :end_date ,:customer_name, :customer_ID, :customer_phone, :customer_address)   
    end

    def edInfoOrder_params
        p params.require(:editInfoOrder).permit(:id, :customer_name, :customer_ID, :customer_phone, :customer_address)
    end
end
