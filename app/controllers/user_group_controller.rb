class UserGroupController < ApplicationController
    def destroy
        unless params[:id]
            render json: {response: 'Xoá không thành công'}, status: 400
            return
        end
        @group = UserGroup.find_by_id(params[:id])
        begin
            @group.destroy
            render json: {response: 'Xoá thành công'}, status: 200
        rescue => exception
            render json:  @group.users.pluck(:user_name).to_json, status: 409
        end
    end
end
