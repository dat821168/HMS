Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "room#index"

  resources :user
  resources :user_group
  resources :order_room
  
  get 'admin/:id/showroominfo', to: 'admin#showroominfo', as: 'showroominfo'
  post 'order_room/createOrder', to: 'order_room#createOrder'
  get 'order_room/:id/selectRoom', to: 'order_room#selectRoom', as: 'selectRoom'
  get 'order_room/:id/viewInfoOrder', to: 'order_room#viewInfoOrder', as: 'viewInfoOrder'
  post 'order_room/editInfoOrder', to: 'order_room#editInfoOrder'  
  #Sort List Order
  get 'order_room/:id/sortOrderCode', to: 'order_room#sortOrderCode'
  get 'order_room/:id/sortRoomCode', to: 'order_room#sortRoomCode'
  get 'order_room/:id/sortUserId', to: 'order_room#sortUserId'
  get 'order_room/:id/sortCreated', to: 'order_room#sortCreated'
  get 'order_room/:id/sortUpdated', to: 'order_room#sortUpdated'

  get 'room', to: 'room#index'
  get 'room/:id/showroominfo', to: 'room#show'
  get 'room/:id/updateRoomStatus/:status', to: 'room#update_room_status'

  # User routes 
  post 'user/update', to: 'user#update'
  get 'user/:id/getProvinces', to: 'user#get_provinces'
  get 'user/:pass/verifypassword', to: 'user#verify_password'

 
end
