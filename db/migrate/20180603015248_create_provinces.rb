class CreateProvinces < ActiveRecord::Migration[5.2]
  def change
    create_table :provinces do |t|
      t.string :province_name, index: true, null: false
      t.string :province_code
      t.timestamps
      t.references :country, index: true, foreign_key: true
    end
  end
end
