class CreateUserPerGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :user_per_groups do |t|
      t.string :description
      t.references :user, index: true, foreign_key: true
      t.references :user_group, index: true, foreign_key: true
      t.timestamps
    end
  end
end
