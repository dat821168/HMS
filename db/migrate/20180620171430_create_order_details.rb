class CreateOrderDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :order_details do |t|
      t.string :order_code, null: false
       t.datetime :start_date, null: false
       t.datetime :end_date
       t.integer :total
       t.string :customer_name, null: false
       t.string :customer_ID, null: false
       t.string :customer_phone
       t.string :customer_address
       t.references :order, index: true, foreign_key: true
       t.timestamps
    end
  end
end
