class CreateRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms do |t|
      t.string :code , null: false
      t.string :status, default: "available"
      t.string :description
      t.timestamps
      t.references :room_type, index: true, foreign_key: true
    end
  end
end
