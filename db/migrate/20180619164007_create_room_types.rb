class CreateRoomTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :room_types do |t|
      t.string  :type_name, null: false
      t.float   :price, null: false
      t.string :description
      t.timestamps
    end
  end
end
